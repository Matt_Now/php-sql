/*Première Partie*/ 

/*1*/

SELECT * FROM lpecom_livres;

/*2*/

SELECT * FROM lpecom_livres WHERE prix > 20 ORDER BY prix DESC;

/*3*/

SELECT * FROM lpecom_livres ORDER BY prix DESC;

/*4*/

SELECT * FROM lpecom_livres ORDER BY prix DESC limit 1;

/*5*/

SELECT * FROM lpecom_livres WHERE prix BETWEEN 20 AND 22 ORDER BY prix DESC;

/*6*/

SELECT * FROM lpecom_livres;
DELETE FROM lpecom_livres WHERE isbn_10 = '2092589547';

/*7*/

SELECT prix AS Minus FROM lpecom_livres ORDER BY prix ASC limit 1;

/*8*/

SELECT * FROM lpecom_livres;
DELETE FROM lpecom_livres WHERE id_livre = '1' order by id_livre ASC limit 5;