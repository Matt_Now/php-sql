/*PARTIE */

/*1*/

SELECT id_etudiant FROM lpecom_examens WHERE id_examen >= 1 order by id_etudiant desc;

/*2*/

SELECT COUNT(id_etudiant) FROM lpecom_examens WHERE id_examen >= 1;

/*3*/

SELECT AVG(note) FROM lpecom_examens WHERE id_examen = 45;

/*4*/

SELECT note FROM lpecom_examens WHERE id_examen = 87 order by id_examen desc limit 1;

/*5*/

SELECT DISTINCT id_etudiant FROM lpecom_examens WHERE (id_examen = 45 AND note > 11) OR (id_examen = 87 AND note > 12);

/*6*/

SELECT ex.*, et.prenom, et.nom FROM lpecom_examens ex LEFT JOIN lpecom_etudiants et ON ex.id_etudiant = et.id_etudiant;

/*7*/

SELECT ex.*, et.prenom, et.nom FROM lpecom_examens ex INNER JOIN lpecom_etudiants et ON ex.id_etudiant = et.id_etudiant;

/*8*/

SELECT et.prenom, et.nom, AVG(ex.note) as moyenne FROM lpecom_examens ex INNER JOIN lpecom_etudiants et ON ex.id_etudiant = et.id_etudiant WHERE et.id_etudiant = 30;

/*9*/

SELECT * FROM lpecom_examens ex INNER JOIN lpecom_etudiants et ON ex.id_etudiant = et.id_etudiant ORDER BY ex.note DESC LIMIT 3;