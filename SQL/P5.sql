/*PARTIE 5*/

/*1*/

SELECT c.* FROM lpecom_covid c WHERE jour = '2021-04-01';

/*2*/

SELECT r.name, c.* FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE jour = '2021-04-01';

/*3*/

SELECT SUM(n_dose1) FROM lpecom_covid c WHERE jour <= '2020-12-31';
SELECT SUM(n_dose2) FROM lpecom_covid c WHERE jour <= '2020-12-31';

/*4*/

SELECT SUM(n_dose1) FROM lpecom_covid c WHERE id_region = '93' AND jour BETWEEN '2021-03-01' AND '2021-03-31';

/*5*/

SELECT SUM(n_dose2) FROM lpecom_covid c WHERE id_region = '11'AND jour BETWEEN '2021-03-01' AND '2021-03-31';

/*6*/

SELECT MAX(n_dose1) FROM lpecom_covid c;
SELECT c.*, r.name FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE c.n_dose1 >= 56661;

/*7*/

SELECT MAX(n_dose2) FROM lpecom_covid c;
SELECT c.*, r.name FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE c.n_dose2 >= 21524;

/*8*/

SELECT MAX(couv_dose1) FROM lpecom_covid c;
SELECT c.*, r.name FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE c.couv_dose1 >= 19.7;
SELECT MAX(couv_dose2) FROM lpecom_covid c;
SELECT c.*, r.name FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE c.couv_dose2 >= 8;

/*9*/

SELECT MIN(c.couv_dose1) FROM lpecom_covid c WHERE c.jour = '2021-04-06';
SELECT c.*, r.name FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE c.jour = '2021-04-06' AND c.couv_dose1 <= 2.80;

/*10*/

SELECT AVG(c.couv_dose1) AS couverture_dose1_avg, AVG(c.couv_dose2) AS couverture_dose2_avg FROM lpecom_covid c WHERE c.jour = '2021-04-06';

/*11*/

SELECT c.*, r.name FROM lpecom_covid c INNER JOIN lpecom_regions r ON c.id_region = r.code WHERE c.couv_dose1 >= 15 AND c.couv_dose2 >= 5 AND c.jour = '2021-04-06';