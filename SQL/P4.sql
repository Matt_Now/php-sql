/*PARTIE 4*/

/*1*/

SELECT * FROM lpecom_cities WHERE gps_lat = 48.66913724637683 AND gps_lng = 1.87586057971015;

/*2*/

SELECT COUNT(*) FROM lpecom_cities WHERE department_code = 91;

/*3*/

SELECT COUNT(*) FROM lpecom_cities WHERE name LIKE "%-le-Roi";

/*4*/

SELECT COUNT(*) as n_cities FROM lpecom_cities WHERE zip_code = 77320;

/*5*/

SELECT COUNT(*) FROM lpecom_cities WHERE name LIKE "SAINT-%" AND department_code = 77;

/*6*/

SELECT * FROM lpecom_cities WHERE zip_code BETWEEN 77210 AND 77810;

/*7*/

SELECT * FROM lpecom_cities WHERE department_code = 77 ORDER BY zip_code DESC LIMIT 2;

/*8*/

SELECT MAX(zip_code) FROM lpecom_cities;

/*9*/

SELECT d.name AS departement, r.name AS region, d.slug FROM lpecom_departments d INNER JOIN lpecom_regions r ON (d.region_code = r.code) WHERE d.region_code IN (75, 27, 53, 84, 93);

/*10*/

SELECT r.name as reg, d.name as dep, c.name as ville FROM lpecom_cities c INNER JOIN lpecom_departments d ON (c.department_code = d.code) INNER JOIN lpecom_regions r ON (d.region_code = r.code) WHERE d.code = 77;