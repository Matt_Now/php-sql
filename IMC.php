    <?php
        class Personne{ /*Création class Personne*/
            public float $taille; /*Création Variable (Float (nombre à virgule)) de la taille*/
            public float $poids; /*Création Variable (Float (nombre à virgule)) du poids*/

            public function __construct(float $taille, float $poids) /*Création Function Construct, la base en PHP*/
            {
                $this->taille = $taille;
                $this->poids = $poids;
            }

            public function getTaille():float{ /*Function pour initier la taille*/
                return $this->taille;
            }

            public function setTaille(float $taille):void{ /*Function pour modifier la taille*/
                $this->taille = $taille;
            }

            public function getPoids():float{ /*Function pour initier le poids*/
                return $this->poids;
            }

            public function setPoids(float $poids):void{ /*Function pour modifier la taille*/
                $this->poids = $poids;
            }

            public function calcul():int{ /*Function de calcul pour l'IMC*/
                return $this->poids / ($this->taille * $this->taille);
            }

            public function IMC(){ /*Function des echos pour l'IMC*/
                $IMC = $this->calcul();
                if($IMC < 18.5){
                    return "Tu as un souspoids";
                }elseif($IMC >= 18.5 && $IMC < 25){
                    return "Tu as un poids normal";
                } else {
                    return "Tu as un surpoids";
                }
            }
        }
        $calcul = new Personne(1.65, 95.7); /*On met la taille et le poids de notre personne*/
        echo "Votre IMC est de : ". $calcul->calcul(); /*On fait le Calcul*/
        echo "Votre status de poids est : ". $calcul->IMC(); /*On donne l'IMC*/